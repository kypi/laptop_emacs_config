#+TITLE: Laptop-~Emacs~-Config

* Introduction
This repo is all *configuration* files of ~Emacs~.

** Content

- *Initialize file*

- *Company mode*
- *Flycheck mode*

- *Lua mode*

- *Merlin*
- *Utop*
- *Tuareg*

- *Markdown mode*
- *YAML mode*